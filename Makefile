PKG := "gitee.com/gongpengABC/common"

gen: ## Init Service
	@protoc -I=. -I=/usr/local/include --go_out=. --go_opt=module=${PKG} --go-grpc_out=. --go-grpc_opt=module=${PKG} pb/*/*.proto
	@protoc-go-inject-tag -input=pb/*/*.pb.go
	@go generate ./...
	@mcube generate enum -p -m pb/*/*.pb.go



